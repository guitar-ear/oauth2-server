from website.resources.user import UserLogin, UserRapidLogin, UserLogout, UserRegister, UserHome
from website.resources.user import UserSecurity, UserAdmin, UserEmailVerification, UserEmailConfirm
from website.resources.user import AvatarImage, AvatarImageRedis, UserCropAvatar

from website.resources.client import NewClient, DeleteClient

from website.resources.jwt import EmitToken, RefreshToken

from website.resources.google import LoginGoogle, AuthGoogle

# Config
ROOT = ''


def config_route(app):
    # USER
    app.add_url_rule(ROOT + '/', view_func=UserLogin.as_view('login'))
    app.add_url_rule(ROOT + '/rapid-login', view_func=UserRapidLogin.as_view('rapid-login'))
    app.add_url_rule(ROOT + '/avatar/<int:img_id>', view_func=AvatarImage.as_view('avatar_img'))
    app.add_url_rule(ROOT + '/avatar/crop/<filename>', view_func=AvatarImageRedis.as_view('avatar_raw'))
    app.add_url_rule(ROOT + '/avatar/crop', view_func=UserCropAvatar.as_view('crop_avatar'))
    app.add_url_rule(ROOT + '/logout', view_func=UserLogout.as_view('logout'))
    app.add_url_rule(ROOT + '/register', view_func=UserRegister.as_view('register'))
    app.add_url_rule(ROOT + '/home', view_func=UserHome.as_view('home'))
    app.add_url_rule(ROOT + '/home/security', view_func=UserSecurity.as_view('security'))
    app.add_url_rule(ROOT + '/home/admin', view_func=UserAdmin.as_view('admin'))
    app.add_url_rule(ROOT + '/email-verification', view_func=UserEmailVerification.as_view('email_verification'))
    app.add_url_rule(ROOT + '/email-confirm/<token>', view_func=UserEmailConfirm.as_view('email_confirm'))

    # CLIENT
    app.add_url_rule(ROOT + '/new-client', view_func=NewClient.as_view('new-client'))
    app.add_url_rule(ROOT + '/delete-client', view_func=DeleteClient.as_view('delete-client'))

    # JWT TOKEN
    app.add_url_rule(ROOT + '/token/emit', view_func=EmitToken.as_view('token-emit'))
    app.add_url_rule(ROOT + '/token/refresh', view_func=RefreshToken.as_view('token-refresh'))

    # GOOGLE LOGIN
    app.add_url_rule(ROOT + '/google/login', view_func=LoginGoogle.as_view('google-login'))
    app.add_url_rule(ROOT + '/google/auth', view_func=AuthGoogle.as_view('google-auth'))
