from abc import ABC
from itsdangerous import BadSignature
from flask.sessions import SecureCookieSession, SecureCookieSessionInterface
from flask import redirect, url_for, session as ses
import hashlib
import json
import os

from website.redis import redis_client


def login_user(scope: str):
    def decorator(func):
        def wrapper(*args, **kwargs):
            if not ses:
                return redirect(url_for('login'))
            if "scope" not in ses:
                CreateUserSession.delete_session()
                return redirect(url_for('login'))
            if scope in ses["scope"]:
                return func(*args, **kwargs)
            if "portal_user_check_email" in ses["scope"]:
                return redirect(url_for('email_verification'))
            return redirect(url_for('login'))

        return wrapper

    return decorator


class CreateUserSession:
    def __init__(self, user):
        ses["id"] = user.id
        ses["scope"] = user.get_scope()
        ses["lifetime"] = user.get_min_lifetime()

    @staticmethod
    def delete_session():
        ses["delete-session"] = True


class _FlaskSession(SecureCookieSession):
    def __init__(self, initial=None, rid=None):
        super().__init__(initial)
        self.rid = rid


class _FlaskSessionInterface(SecureCookieSessionInterface, ABC):
    session_class = _FlaskSession

    def open_session(self, app, request):
        s = self.get_signing_serializer(app)
        if s is None:
            return None
        val = request.cookies.get(app.session_cookie_name)
        if not val:
            return self.session_class()
        try:
            data = s.loads(val)
            return self.session_class(_RedisSession.get_session(data["rid"]), data["rid"])
        except BadSignature:
            return self.session_class()

    def save_session(self, app, session, response):
        domain = self.get_cookie_domain(app)
        path = self.get_cookie_path(app)

        # If the session is modified to be empty, remove the cookie.
        # If the session is empty, return without setting the cookie.
        if not session:
            if session.modified:
                _RedisSession.del_session(session)
                response.delete_cookie(
                    app.session_cookie_name, domain=domain, path=path
                )

            return

        if "nav-out" in session:
            del session["nav-out"]
            if "client-redirect" in session:
                del session["client-redirect"]
            _RedisSession.set_session(session)
            from website.resources.jwt import generate_barrier_token
            response.set_cookie(
                '_barrier',
                json.dumps(generate_barrier_token()),
                domain=domain
            )
            return

        if "delete-session" in session:
            _RedisSession.del_session(session)
            response.delete_cookie(
                app.session_cookie_name, domain=domain, path=path
            )
            return

        # Add a "Vary: Cookie" header if the session was accessed at all.
        if session.accessed:
            response.vary.add("Cookie")

        if not self.should_set_cookie(app, session):
            return

        httponly = self.get_cookie_httponly(app)
        secure = self.get_cookie_secure(app)
        samesite = self.get_cookie_samesite(app)
        expires = self.get_expiration_time(app, session)
        dump = _RedisSession.set_session(session)
        val = self.get_signing_serializer(app).dumps(dump)
        response.set_cookie(
            app.session_cookie_name,
            val,
            expires=expires,
            httponly=httponly,
            domain=domain,
            path=path,
            secure=secure,
            samesite=samesite,
        )


class _RedisSession:
    rid = None
    ex = -1

    @staticmethod
    def get_session(_hash: str) -> dict:
        _RedisSession.rid = _hash
        try:
            _RedisSession.ex = redis_client.ttl(_hash)
            return json.loads(redis_client.get(_hash).decode())
        except AttributeError:
            return {}

    @staticmethod
    def set_session(session):
        if "lifetime" in session:
            _RedisSession.ex = session["lifetime"]
            del session["lifetime"]
        dump = json.dumps(dict(session))
        if not session.rid:
            _hash = hashlib.sha1(dump.encode() + os.urandom(32)).hexdigest()
            _RedisSession.ex = 300  # 5min
        else:
            _hash = session.rid
        if not _RedisSession.ex or _RedisSession.ex < 0:
            _RedisSession.ex = 300  # 5min
        redis_client.set(_hash, dump, ex=_RedisSession.ex)
        return {"rid": _hash}

    @staticmethod
    def del_session(session):
        redis_client.delete(session.rid)
