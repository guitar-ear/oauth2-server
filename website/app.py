import os
from flask import Flask
from flask_bootstrap import Bootstrap

from .db import db
from .migrate import migrate
from .redis import redis_client
from .session import _FlaskSessionInterface
from .jwt import jwt
from .oauth_client import oauth_client
from .routes import config_route
from .avatars import avatars


def create_app(config=None):
    app = Flask(__name__)

    # load default configuration
    app.config.from_object('website.settings')

    # load environment configuration
    if 'WEBSITE_CONF' in os.environ:
        app.config.from_envvar('WEBSITE_CONF')

    # load app specified configuration
    if config is not None:
        if isinstance(config, dict):
            app.config.update(config)
        elif config.endswith('.py'):
            app.config.from_pyfile(config)

    setup_app(app)
    return app


def setup_app(app):
    # Create tables if they do not exist already
    @app.before_first_request
    def create_tables():
        db.create_all()

    # INIT POSTGRES
    db.init_app(app)
    migrate.init_app(app, db)

    # INIT REDIS
    redis_client.init_app(app)

    # INIT SESSION
    app.session_interface = _FlaskSessionInterface()
    app.session_cookie_name = "_portal"

    # INIT JWT
    jwt.init_app(app)

    # OAUTH CLIENT (login with google)
    oauth_client.init_app(app)

    # ENDPOINTS
    config_route(app)

    # BOOTSTRAP CONFIG
    Bootstrap(app)

    # INIT AVATARS
    avatars.init_app(app)
