from flask import session, current_app
from flask_jwt_extended import JWTManager
from flask_jwt_extended.view_decorators import _decode_jwt_from_headers
from flask_jwt_extended.exceptions import RevokedTokenError
import jwt as jat

from website.session import _RedisSession

jwt = JWTManager()


def extract_session_from_jwt_rid():
    try:
        encoded_token, _ = _decode_jwt_from_headers()
    except Exception:
        return None
    unverified_claims = jat.decode(
        encoded_token, verify=False, algorithms=current_app.config['JWT_ALGORITHM']
    )
    return _RedisSession.get_session(unverified_claims["identity"])


@jwt.decode_key_loader
def key_decode(claims, headers):
    if "jwt-secret" not in session:
        response = extract_session_from_jwt_rid()
        if "jwt-secret" in response:
            return response["jwt-secret"]
        raise RevokedTokenError
    return session["jwt-secret"]


@jwt.encode_key_loader
def key_encode(identity):
    if "jwt-secret" not in session:
        response = extract_session_from_jwt_rid()
        if "jwt-secret" in response:
            return response["jwt-secret"]
        raise RevokedTokenError
    return session["jwt-secret"]
