from flask import url_for, redirect, current_app
from flask.views import MethodView
from flask import session
from datetime import datetime
from PIL import Image
import requests
import os
import io

from secret import GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET
from website.models.photo import Photo
from website.oauth_client import oauth_client
from website.models.user import User
from website.resources.jwt import generate_barrier_token
from website.session import CreateUserSession

google = oauth_client.register(
    'google',
    client_id=GOOGLE_CLIENT_ID,
    client_secret=GOOGLE_CLIENT_SECRET,
    access_token_url='https://www.googleapis.com/oauth2/v4/token',
    authorize_url='https://accounts.google.com/o/oauth2/v2/auth?access_type=offline',
    api_base_url='https://www.googleapis.com/',
    server_metadata_url='https://accounts.google.com/.well-known/openid-configuration',
    client_kwargs={'scope': 'openid email profile'},
)


class LoginGoogle(MethodView):
    def get(self):
        redirect_uri = url_for('google-auth', _external=True, _scheme=current_app.config['FLASK_HTTPS'])
        return oauth_client.google.authorize_redirect(redirect_uri)


class AuthGoogle(MethodView):
    def get(self):
        token = oauth_client.google.authorize_access_token()
        # google_session = OAuth2Session(GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET, token=token)
        user_info = oauth_client.google.parse_id_token(token)
        user = User.find_by_email(user_info.email)
        if not user:
            user = User(
                user_info.email,
                user_info.name,
                str(os.urandom(24)),
                user_info.given_name,
                user_info.family_name
            )

            user.email_submitted_on = datetime.now()
            user.email_confirmed = True

            # Set privilege
            user.set_privilege_basic_user()

            # Get Google Avatar
            img_byte_arr = io.BytesIO()
            img_byte_arr.write(requests.get(user_info.picture).content)
            user.photo = Photo(img_byte_arr.getvalue())

            user.save_to_db()

        # Success
        CreateUserSession(user)

        # Nav to client
        if "client-redirect" in session:
            session["nav-out"] = True
            return redirect(session["client-redirect"], code=302)

        return redirect(url_for('home'))
