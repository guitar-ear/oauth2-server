from flask import request, redirect, render_template, url_for
from flask.views import MethodView
from flask_login import login_required, current_user
from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField
from wtforms.validators import DataRequired
from functools import wraps
from time import time
from werkzeug.security import gen_salt

from website.models.user import User

from website.models.oauth2 import OAuth2Client


# def login_admin(func):
#     @wraps(func)
#     def wrapper(*args, **kwargs):
#         result = login_required(func)
#         try:
#             user = User.find_by_id(current_user.id)
#             if not user.confirmed or user.role.value[0] != "admin":
#                 return redirect(url_for('email_verification'))
#             return result(func)
#         except:
#             return result(func)
#
#     return wrapper


def split_by_crlf(s):
    return [v for v in s.splitlines() if v]


class NewClientForm(FlaskForm):
    client_name = StringField('Client Name', validators=[DataRequired()])
    client_uri = StringField('Client URI', validators=[DataRequired()])
    grant_types = TextAreaField('Grant Types', validators=[DataRequired()])
    redirect_uris = TextAreaField('Redirect Uris', validators=[DataRequired()])
    response_types = TextAreaField('Response Types', validators=[DataRequired()])
    scope = StringField('Scope', validators=[DataRequired()])
    token_endpoint_auth_method = StringField('Token Endpoint Auth Method', validators=[DataRequired()])


class NewClient(MethodView):
    # @login_admin
    def get(self):
        return render_template('client/new_client.html', form=NewClientForm())

    # @login_admin
    def post(self):
        client_id = gen_salt(24)
        client_id_issued_at = int(time())
        client = OAuth2Client(
            client_id=client_id,
            client_id_issued_at=client_id_issued_at,
            user_id=current_user.id,
        )

        if client.token_endpoint_auth_method == 'none':
            client.client_secret = ''
        else:
            client.client_secret = gen_salt(48)

        form = NewClientForm()
        if form.validate_on_submit():
            client_metadata = {
                "client_name": form["client_name"].data,
                "client_uri": form["client_uri"].data,
                "grant_types": split_by_crlf(form["grant_types"].data),
                "redirect_uris": split_by_crlf(form["redirect_uris"].data),
                "response_types": split_by_crlf(form["response_types"].data),
                "scope": form["scope"].data,
                "token_endpoint_auth_method": form["token_endpoint_auth_method"].data
            }
            client.set_client_metadata(client_metadata)
            client.save_to_db()

            return redirect(url_for('admin'))
        return render_template('client/new_client.html', form=form)


class DeleteClient(MethodView):
    # @login_admin
    def post(self):
        form = request.form
        if form["id"]:
            client = OAuth2Client.find_by_id(form["id"])
            if client:
                client.delete_from_db()
        return redirect(url_for('admin'))
