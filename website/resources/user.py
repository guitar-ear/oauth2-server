from flask import redirect, request, render_template, url_for, session, send_file
from flask.views import MethodView
from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed
from wtforms import PasswordField, StringField, FileField, BooleanField, HiddenField, SubmitField, validators
from datetime import datetime
from time import ctime
from PIL import Image, ImageOps
import requests
import hashlib
import array
import re
import io

from website.models.user import User
from website.models.photo import Photo
from website.models.oauth2 import OAuth2Client
from website.helpers.email import EmailTool, EmailProvider
from website.avatars import avatars

from website.session import CreateUserSession, login_user


class LoginForm(FlaskForm):
    email = StringField('Email Address', [validators.Length(min=6, max=255)])
    password = PasswordField('Password', [validators.DataRequired(), validators.Length(min=6, max=60)])


class AvatarImage(MethodView):
    def get(self, img_id):
        image = Photo.find_by_id(img_id)
        if not image:
            return None
        return send_file(
            io.BytesIO(image.photo),
            attachment_filename='avatar.jpeg',
            mimetype='image/jpg'
        )


class AvatarImageRedis(MethodView):
    def get(self, filename):
        if "avatar-original" in session:
            return send_file(
                io.BytesIO(bytes(memoryview(array.array('H', session["avatar-original"])))),
                attachment_filename=filename,
                mimetype='image/jpg'
            )


class UserLogin(MethodView):
    def get(self):
        nav_out = request.args.get("redirect")
        if nav_out:
            session["client-redirect"] = nav_out
            return redirect(url_for('login'))
        user = None
        if "id" in session:
            user = User.find_by_id(session["id"])
        return render_template("user/login.html", form=LoginForm(), user=user)

    def post(self):
        form = LoginForm()
        if form.validate_on_submit():
            # Check if the user exist
            user = User.find_by_email(form.email.data)
            if not user:
                return render_template("user/login.html", form=form, error="Invalid username/password.")

            # Check for the correct user password
            if not user.check_password(form.password.data):
                return render_template("user/login.html", form=form, error="Invalid username/password.")

            # Success
            CreateUserSession(user)

            # Nav to client
            if "client-redirect" in session:
                session["nav-out"] = True
                return redirect(session["client-redirect"], code=302)

            return redirect(url_for('home'))

        user = None
        if "id" in session:
            user = User.find_by_id(session["id"])

        return render_template("user/login.html", form=form, error="Invalid username/password.", user=user)


class UserRapidLogin(MethodView):
    @login_user("portal_user_home")
    def get(self):
        # Nav to client
        if "client-redirect" in session:
            session["nav-out"] = True
            return redirect(session["client-redirect"], code=302)
        return redirect(url_for('home'))


class UserLogout(MethodView):
    @login_user("portal_user_logout")
    def get(self):
        CreateUserSession.delete_session()
        return redirect(url_for('login'))


class RegistrationForm(FlaskForm):
    username = StringField('Username *', [validators.Length(min=4, max=250), validators.DataRequired()])
    email = StringField('Email Address *', [validators.Length(min=6, max=255), validators.DataRequired()])
    password = PasswordField('New Password *', [validators.DataRequired(), validators.Length(min=6, max=60)])
    confirm = PasswordField('Repeat Password *', [validators.DataRequired()])
    first_name = StringField('First Name *', [validators.DataRequired()])
    middle_name = StringField('Middle Name')
    last_name = StringField('Last Name *', [validators.DataRequired()])
    photo = FileField('Profile picture', [FileAllowed(['jpg', 'png', 'gif'], 'Images only!')])
    accept_tos = BooleanField('I accept the Term of service *', [validators.DataRequired()])


class UserRegister(MethodView):
    def get(self):
        return render_template("user/register.html", form=RegistrationForm())

    def post(self):
        form = RegistrationForm()
        if form.validate_on_submit():
            # Check if the email is valid
            valid_email = re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", form.email.data)
            if not valid_email:
                return render_template("user/register.html", form=form, error="The email is not valid")

            # Check if the password match
            if form.password.data != form.confirm.data:
                return render_template("user/register.html", form=form, error="The passwords do not match!")

            # Check if the user already exist
            if User.find_by_email(form.email.data) or User.find_by_username(form.username.data):
                return render_template(
                    "user/register.html",
                    form=form,
                    error="A user with this email already exist"
                )

            # Send confirm email
            SendConfirmEmail(form.email.data, form.first_name.data, form.last_name.data)

            # Create user
            user = User(
                form.email.data,
                form.username.data,
                form.password.data,
                form.first_name.data,
                form.middle_name.data,
                None if form.middle_name.data.strip() == '' else form.middle_name.data
            )
            user.email_submitted_on = datetime.now()

            # Check for image
            if form.photo.data:
                im = Image.open(form.photo.data)
                if im.size[0] == im.size[1] and im.size[0] <= 400:
                    im = ImageOps.pad(im, (180, 180))
                img_byte_arr = io.BytesIO()
                im.save(img_byte_arr, format="JPEG")
                if im.size[0] == 180 and im.size[1] == 180:
                    user.photo = Photo(img_byte_arr.getvalue())
                else:
                    session["avatar-original"] = list(map(lambda x: x, memoryview(img_byte_arr.getvalue()).cast('H')))
            else:
                avatar_hash = hashlib.md5(user.email.lower().encode('utf-8')).hexdigest()
                img_byte_arr = io.BytesIO()
                img_byte_arr.write(requests.get(avatars.gravatar(avatar_hash, size=180)).content)
                user.photo = Photo(img_byte_arr.getvalue())

            # Set privilege
            user.set_privilege_email_confirm()

            user.save_to_db()

            # Session
            CreateUserSession(user)

            if "avatar-original" in session:
                return redirect(url_for('crop_avatar'))
            return redirect(url_for('email_verification'))

        return render_template("user/register.html", form=form)


class CropAvatarForm(FlaskForm):
    x = HiddenField()
    y = HiddenField()
    w = HiddenField()
    h = HiddenField()


class UserCropAvatar(MethodView):
    def get(self):
        if "avatar-original" not in session:
            return redirect(url_for('email_verification'))
        return render_template('user/crop_avatar.html', form=CropAvatarForm())

    def post(self):
        form = CropAvatarForm()
        if form.validate_on_submit():
            # Resize Info
            x = int(form.x.data)
            y = int(form.y.data)
            w = int(form.w.data)
            h = int(form.h.data)

            # Image Manipulation
            try:
                image = bytes(memoryview(array.array('H', session["avatar-original"])))
            except KeyError:
                return redirect(url_for('email_verification'))
            raw_img = Image.open(io.BytesIO(image)).convert('RGB')
            cropped_img = raw_img.crop((x, y, x + w, y + h))
            avatar = ImageOps.pad(cropped_img, (180, 180))

            # Image to DB
            img_byte_arr = io.BytesIO()
            avatar.save(img_byte_arr, "JPEG")
            user = User.find_by_id(session["id"])
            user.photo = Photo(img_byte_arr.getvalue())
            user.save_to_db()

            if "avatar-original" in session:
                del session["avatar-original"]

            return redirect(url_for('email_verification'))
        return redirect(url_for('crop_avatar'))


class SendConfirmEmail:
    def __init__(self, email: str, first_name: str, last_name: str):
        token = EmailTool.generate_confirmation_token(email)
        confirm_url = url_for('email_confirm', token=token, _external=True)
        html = render_template(
            'user/email_confirm_page.html',
            confirm_url=confirm_url,
            first_name=first_name,
            last_name=last_name
        )
        subject = "Welcome to Guitar Ear, Please confirm your Email"
        EmailProvider.send_email(email, subject, html)


class UserEmailVerification(MethodView):
    @login_user("portal_user_check_email")
    def get(self):
        return render_template('user/email_verification.html', email=User.find_by_id(session["id"]).email, post=False)

    @login_user("portal_user_check_email")
    def post(self):
        user = User.find_by_id(session["id"])
        SendConfirmEmail(user.email, user.first_name, user.last_name)
        user.email_submission = datetime.now()
        user.save_to_db()
        return render_template('user/email_verification.html', email=user.email, post=True)


class UserEmailConfirm(MethodView):
    @login_user("portal_user_confirm_email")
    def get(self, token):
        email = EmailTool.confirm_token(token)
        if not email:
            return redirect(url_for('email_verification'))

        user = User.find_by_email(email=email)
        if user.email_confirmed:
            redirect(url_for('home'))
        else:
            # Update privilege
            user.confirmed = True
            user.set_privilege_basic_user()
            user.save_to_db()

            # Update Redis Session
            CreateUserSession(user)
        return redirect(url_for('home'))


class UserHome(MethodView):
    @login_user("portal_user_home")
    def get(self):
        user = User.find_by_id(session["id"])
        is_admin = 'admin_privilege_page' in session["scope"]
        return render_template('user/home.html', user=user, role=is_admin)


class UserSecurity(MethodView):
    @login_user("portal_user_security")
    def get(self):
        user = User.find_by_id(session["id"])
        is_admin = 'admin_privilege_page' in session["scope"]
        return render_template('user/home.html', security=user, role=is_admin)


class UserAdmin(MethodView):
    @login_user("admin_privilege_page")
    def get(self):
        return "Not implemented"
        # user = User.find_by_id(session["id"])
        # return render_template(
        #     'user/home.html',
        #     admin=OAuth2Client.find_by_user_id(user.id),
        #     role=user.role.value[0],
        #     time=ctime
        # )
