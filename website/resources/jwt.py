from flask import jsonify, session
from flask.views import MethodView
from flask_jwt_extended import (
    create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity
)
from datetime import timedelta

from website.session import login_user, _RedisSession
import hashlib
import os


def generate_barrier_token():
    session["jwt-secret"] = hashlib.sha512(os.urandom(128)).hexdigest()
    return {
        'access_token': create_access_token(identity=_RedisSession.rid, expires_delta=timedelta(minutes=20)),
        'refresh_token': create_refresh_token(identity=_RedisSession.rid)
    }


class EmitToken(MethodView):
    @login_user('portal_emit_token')
    def get(self):
        return jsonify(generate_barrier_token()), 200


class RefreshToken(MethodView):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        ret = {
            'access_token': create_access_token(identity=current_user, expires_delta=timedelta(minutes=20))
        }
        return jsonify(ret), 200
