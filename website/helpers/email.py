from itsdangerous import URLSafeTimedSerializer
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

from secret import SECRET_KEY, SECURITY_PASSWORD_SALT, MAIL_DEFAULT_SENDER, SENDGRID_API_KEY


class EmailTool:
    @staticmethod
    def generate_confirmation_token(email: str):
        serializer = URLSafeTimedSerializer(SECRET_KEY)
        return serializer.dumps(email, salt=SECURITY_PASSWORD_SALT)

    @staticmethod
    def confirm_token(token, expiration=3600):
        serializer = URLSafeTimedSerializer(SECRET_KEY)
        try:
            email = serializer.loads(
                token,
                salt=SECURITY_PASSWORD_SALT,
                max_age=expiration
            )
        except Exception:
            return False
        return email


class EmailProvider:
    @staticmethod
    def send_email(_to, subject, template):
        message = Mail(
            from_email=MAIL_DEFAULT_SENDER,
            to_emails=_to,
            subject=subject,
            html_content=template
        )
        try:
            sg = SendGridAPIClient(api_key=SENDGRID_API_KEY)
            sg.send(message)
            # response = sg.send(message)
            # print(response.status_code)
            # print(response.body)
            # print(response.headers)
        except Exception as e:
            print(e.message)
