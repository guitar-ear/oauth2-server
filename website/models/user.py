import bcrypt
from datetime import datetime
from website.db import db

from website.models.role import Role
from website.models.photo import Photo

user_role = db.Table(
    'user_role',
    db.Column('fk_user', db.Integer, db.ForeignKey('user.id', ondelete="CASCADE"), primary_key=True),
    db.Column('fk_role', db.Integer, db.ForeignKey('role.id', ondelete="CASCADE"), primary_key=True),
    db.Column('created_on', db.DateTime, default=datetime.now(), nullable=False),
)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    username = db.Column(db.String(255), nullable=False)
    password = db.Column(db.String(60), nullable=False)
    roles = db.relationship(
        'Role',
        secondary=user_role,
        lazy='subquery'
    )

    first_name = db.Column(db.String(255))
    middle_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))

    photo_id = db.Column(db.Integer, db.ForeignKey('photo.id'))
    photo = db.relationship("Photo", single_parent=True, cascade="all, delete-orphan")

    created_on = db.Column(db.DateTime, default=datetime.now(), nullable=False)
    updated_on = db.Column(db.DateTime)

    email_submitted_on = db.Column(db.DateTime)
    email_confirmed = db.Column(db.Boolean, default=False)

    def __init__(self, email: str, username: str, password: str, first_name: str, last_name: str, middle_name=None):
        self.email = email
        self.username = username
        self.password = bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()

        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name

    def __str__(self):
        return f"STR: {self.email}, {self.password}"

    def save_to_db(self):
        self.updated_on = datetime.now()
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    def get_id(self):
        return self.id

    def get_roles(self):
        return [r.name for r in self.roles]

    def get_scope(self):
        return list(set([action.name for role in self.roles for action in role.actions]))

    def get_min_lifetime(self):
        life = list(map(lambda role: role.lifetime, self.roles))
        life.sort()
        return None if not self.roles else life[0]

    def check_password(self, password):
        return bcrypt.checkpw(password.encode(), self.password.encode())

    def set_privilege_email_confirm(self):
        role = Role.find_by_name("Confirm Account")
        if not role:
            return
        self.roles.clear()
        self.roles.append(role)

    def set_privilege_basic_user(self):
        role = Role.find_by_name("Basic User Portal")
        if not role:
            return
        self.roles.clear()
        self.roles.append(role)

    @classmethod
    def find_by_email(cls, email: str):
        return cls.query.filter_by(email=email).first()

    @classmethod
    def find_by_id(cls, _id: int):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_by_username(cls, username: str):
        return cls.query.filter_by(username=username).first()
