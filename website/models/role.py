from website.db import db

from website.models.action import Action

role_action = db.Table(
    'role_action',
    db.Column('fk_role', db.Integer, db.ForeignKey('role.id', ondelete="CASCADE"), primary_key=True),
    db.Column('fk_action', db.Integer, db.ForeignKey('action.id', ondelete="CASCADE"), primary_key=True)
)


class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True, nullable=False)
    lifetime = db.Column(db.Integer, default=3600, nullable=False)

    actions = db.relationship(
        'Action',
        secondary=role_action,
        lazy='subquery'
    )

    def __init__(self, name: str):
        self.name = name

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def find_by_name(cls, name):
        return cls.query.filter_by(name=name).first()
