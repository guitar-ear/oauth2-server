import os

# SECRET KEYS
SECRET_KEY = os.environ.get('SECRET_KEY', None)
SECURITY_PASSWORD_SALT = os.environ.get('SECURITY_PASSWORD_SALT', None)
FLASK_HTTPS = os.environ.get('FLASK_HTTPS', 'https')

# DB CONFIG
PG_HOST = os.environ.get('PG_HOST', None)  # 127.0.0.1
PG_PORT = os.environ.get('PG_PORT', None)  # 5432
PG_USER = os.environ.get('PG_USER', None)  # postgres
PG_PW = os.environ.get('PG_PW', None)  # password
PG_DB = os.environ.get('PG_DB', None)  # oAuth
POSTGRES_URL = f'postgresql+psycopg2://{PG_USER}:{PG_PW}@{PG_HOST}:{PG_PORT}/{PG_DB}'

# REDIS CONFIG
REDIS_URI = os.environ.get('REDIS_URI', None)  # localhost:6379
REDIS_PW = os.environ.get('REDIS_PW', None)
REDIS_URL = f'redis://:{REDIS_PW}@{REDIS_URI}/0'

# G-MAIL AUTH
MAIL_DEFAULT_SENDER = '	no-reply@em2578.guitarear.live'
SENDGRID_API_KEY = os.environ.get('SENDGRID_API_KEY', None)

# GOOGLE AUTH
GOOGLE_CLIENT_ID = os.environ.get("GOOGLE_CLIENT_ID", None)
GOOGLE_CLIENT_SECRET = os.environ.get("GOOGLE_CLIENT_SECRET", None)
